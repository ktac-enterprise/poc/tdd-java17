package com.kouasseu.portfolio;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 05:36<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class PortfolioPositionTest {

    private static final String MSFT = "MSFT";
    private static final String AAPL = "AAPL";
    private static final String TSLA = "TSLA";
    private static final String ORCL = "ORCL";

    @ParameterizedTest
    @MethodSource("correctPositionCount")
    public void portfolio_shouldHaveCorrectCount(int expectedCount, Portfolio portfolio) {
        Assertions.assertEquals(expectedCount, portfolio.getPositionsSize());
    }

    static Stream<Arguments> correctPositionCount() {
        return Stream.of(
                Arguments.of(0, buildPortfolioWithPosition()),
                Arguments.of(1, buildPortfolioWithPosition(
                        position(MSFT, 10, 260)
                )),
                Arguments.of(2, buildPortfolioWithPosition(
                        position(MSFT, 10, 260),
                        position(AAPL, 2, 150)
                )),
                Arguments.of(1, buildPortfolioWithPosition(
                        position(MSFT, 10, 260),
                        position(MSFT, 5, 200)
                )),
                Arguments.of(4, buildPortfolioWithPosition(
                        position(MSFT, 10, 260),
                        position(MSFT, 5, 200),
                        position(AAPL, 6, 160),
                        position(AAPL, 3, 300),
                        position(ORCL, 6, 160),
                        position(TSLA, 3, 2455)
                ))
        );
    }

    static Portfolio buildPortfolioWithPosition(Position... positions) {
        var portfolio = new Portfolio();
        Arrays.stream(positions).forEach(portfolio::add);
        return portfolio;
    }

    @Test
    public void portfolioWithOnePosition_ReturnsThatPosition() {

        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 10, 260));

        Assertions.assertEquals(10, portfolio.getPosition(MSFT).getQty());
        Assertions.assertEquals(260, portfolio.getPosition(MSFT).getPx());
        Assertions.assertEquals(2600, portfolio.getPosition(MSFT).getValue());
    }

    public static Position position(String symbol, int qty, double px) {
        return new Position(new Stock(symbol), qty, px);
    }

    @Test
    public void portfolioWithTwoDifferentPositions_ReturnsThosePositions() {
        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 10, 260));
        portfolio.add(position(AAPL, 2, 150));

        //msft
        var microsoftPosition = portfolio.getPosition(MSFT);
        Assertions.assertEquals(10, microsoftPosition.getQty());
        Assertions.assertEquals(260, microsoftPosition.getPx());
        Assertions.assertEquals(2600, microsoftPosition.getValue());

        //aapl
        var appplePosition = portfolio.getPosition(AAPL);
        Assertions.assertEquals(2, appplePosition.getQty());
        Assertions.assertEquals(150, appplePosition.getPx());
        Assertions.assertEquals(300, appplePosition.getValue());
    }

    @Test
    public void portfolioWithSameStock_ReturnsCorrectQty() {
        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 10, 260));
        portfolio.add(position(MSFT, 1, 200));

        Assertions.assertEquals(11, portfolio.getPosition(MSFT).getQty());
    }


    @Test
    public void portfolioWithSameStock_ReturnsCorrectAveragePrice() {
        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 1, 260));
        portfolio.add(position(MSFT, 1, 220));

        Assertions.assertEquals(240, portfolio.getPosition(MSFT).getPx());
    }


    @Test
    public void portfolioWithSameStock_ReturnsCorrectPositionValue() {
        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 2, 240));
        portfolio.add(position(MSFT, 1, 220));

        double expected = 2 * 240 + 220;

        Assertions.assertEquals(expected, portfolio.getPosition(MSFT).getValue());
    }


    @Test
    public void complexPortfolio_ReturnsCorrectTotalValue() {
        var portfolio = new Portfolio();

        portfolio.add(position(MSFT, 1, 260));
        portfolio.add(position(MSFT, 2, 250));

        portfolio.add(position(AAPL, 5, 90));
        portfolio.add(position(AAPL, 10, 80));

        portfolio.add(position(ORCL, 100, 80));

        Assertions.assertEquals(10010.0, portfolio.getTotalValue());
    }

    @ParameterizedTest
    @MethodSource("portfolioWithInvalidPositionValue")
    public void portfolioWithInvalidPositionValue_ThrownException(Position position) {
        var portfolio = new Portfolio();
        Assertions.assertThrows(IllegalArgumentException.class, () -> portfolio.add(position));
    }

    static Stream<Arguments> portfolioWithInvalidPositionValue() {
        return Stream.of(
                (Arguments) null,
                Arguments.of(position(null, 10, 234)),
                Arguments.of(position(MSFT, 0, 230)),
                Arguments.of(position(AAPL, 12, 0)),
                Arguments.of(new Position(null, 12, 235))
        );
    }

    @Test
    public void portfolioRemoveStock_ReturnCorrectValue() {
        var portfolio = buildPortfolioWithPosition(
                position(MSFT, 10, 260),
                position(AAPL, 10, 140),
                position(MSFT, 5, 150),
                position(AAPL, 15, 120),
                position(MSFT, 5, 270),
                position(TSLA, 20, 250),
                position(ORCL, 15, 200)
        );

        portfolio.remove(MSFT, 10, 100);

        Assertions.assertEquals(10, portfolio.getPosition(MSFT).getQty());
        Assertions.assertEquals(370, portfolio.getPosition(MSFT).getPx());
        Assertions.assertEquals(3700, portfolio.getPosition(MSFT).getValue());
    }

    @Test
    public void portfolioRemoveAllPositionQuantity_ReturnPortfolioWithoutThatPosition() {
        var portfolio = buildPortfolioWithPosition(
                position(MSFT, 10, 260),
                position(AAPL, 10, 140),
                position(MSFT, 5, 150),
                position(AAPL, 15, 120),
                position(MSFT, 5, 270),
                position(TSLA, 20, 250),
                position(ORCL, 15, 200)
        );

        portfolio.remove(AAPL, 25, 300);

        Assertions.assertEquals(3, portfolio.getPositionsSize());
    }

    @ParameterizedTest
    @MethodSource("positionToRemovedWithInvalidValue")
    public void portfolioRemovePositionWithInvalidValue_ThrownException(String stock, int qty, int px) {
        var portfolio = new Portfolio();
        Assertions.assertThrows(IllegalArgumentException.class, () -> portfolio.remove(stock, qty, px));
    }

    static Stream<Arguments> positionToRemovedWithInvalidValue() {
        return Stream.of(
                Arguments.of((Arguments) null, 10, 300),
                Arguments.of("", 10, 300),
                Arguments.of("XXX", 10, 300),
                Arguments.of(MSFT, 0, 344),
                Arguments.of(MSFT, 30, 344),
                Arguments.of(MSFT, 123, 0)
        );
    }
}
