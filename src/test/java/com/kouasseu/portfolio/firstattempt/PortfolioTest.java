package com.kouasseu.portfolio.firstattempt;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 04:15<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class PortfolioTest {

    @Test
    public void emptyPortfolio_hasZeroValue() {

        var portfolio = new Portfolio();
        Assertions.assertEquals(0, portfolio.totalValue());
    }

    @Test
    public void portfolioWithOneStock_calculatesTotalValue()  {

        int qty = 10;
        int px = 260;
        double value = qty * px;

        var portfolio = new Portfolio();
        portfolio.add(new Stock("MSFT", qty, px));
        Assertions.assertEquals(value, portfolio.totalValue());
    }

    @Test
    public void portfolioWithMultipleStocks_calculatesTotalValue() {

        // Stock 1
        int microsoftQty = 10;
        int microsoftPx = 260;
        double microsoftValue = microsoftQty * microsoftPx;

        // Stock 1
        int appleQty = 1;
        int applePx = 150;
        double appleValue = appleQty * applePx;

        var portfolio = new Portfolio();
        portfolio.add(new Stock("MSFT", microsoftQty, microsoftPx));
        portfolio.add(new Stock("AAPL", appleQty, applePx));

        Assertions.assertEquals(microsoftValue + appleValue, portfolio.totalValue());
    }


    @Test
    public void portfolioWithSameStocks_calculatesAvgValue() {

        // Stock 1
        int microsoftQty = 10;
        int microsoftPx = 260;
        double microsoftValue = microsoftQty * microsoftPx;

        // Stock 2
        int appleQty = 1;
        double applePx = 120.0;

        // Stock 3
        int appleQty1 = 2;
        double applePx1 = 150.0;
        double appleValue = (appleQty + appleQty1) * (applePx + applePx1)/(appleQty + appleQty1);

        var portfolio = new Portfolio();
        portfolio.add(new Stock("MSFT", microsoftQty, microsoftPx));
        portfolio.add(new Stock("AAPL", appleQty, applePx));
        portfolio.add(new Stock("AAPL", appleQty1, applePx1));

        Assertions.assertEquals(microsoftValue + appleValue, portfolio.totalValue());
    }
}
