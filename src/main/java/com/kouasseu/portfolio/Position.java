package com.kouasseu.portfolio;

import java.util.Objects;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 05:38<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class Position {

    Stock stock;
    int qty;
    double px;

    public Position(Stock stock, int qty, double px) {
        this.stock = stock;
        this.qty = qty;
        this.px = px;
    }

    public Stock getStock() {
        return stock;
    }

    public int getQty() {
        return qty;
    }

    public double getPx() {
        return px;
    }

    public double getValue() {
        return qty * px;
    }

    public void setQuantity(int newQuantity) {
        this.qty = newQuantity;
    }

    public void setPrice(double price) {
        this.px = price;
    }

    @Override
    public String toString() {
        return String.format("{ %s | Qty: %s | Px: %s | Value: %s }", Objects.isNull(stock) ? "null stock" : stock.symbol(), qty, px, qty * px);
    }
}
