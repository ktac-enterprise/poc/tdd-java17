package com.kouasseu.portfolio;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 05:42<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public record Stock(String symbol) {
}
