package com.kouasseu.portfolio;


import java.util.Arrays;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 04:50<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class PortfolioTestDrive {
    private static final String MSFT = "MSFT";
    private static final String AAPL = "AAPL";
    private static final String TSLA = "TSLA";
    private static final String ORCL = "ORCL";
    public static void main(String[] args) {
        /*var portfolio = new Portfolio();

        portfolio.add(position("MFST", 1, 260));
        portfolio.add(position("MFST", 2, 250));
        portfolio.add(position("AAPL", 5, 90));
        portfolio.add(position("AAPL", 10, 80));
        portfolio.add(position("ORCL", 100, 80));*/

        var portfolio = buildPortfolioWithPosition(
                position(MSFT, 10, 260),
                position(AAPL, 10, 140),
                position(MSFT, 5, 150),
                position(AAPL, 15, 120),
                position(MSFT, 5, 270),
                position(TSLA, 20, 250),
                position(ORCL, 15, 200)
        );

        portfolio.print();
    }

    public static Position position(String symbol, int qty, double px) {
        return new Position(new Stock(symbol), qty, px);
    }

    public static Portfolio buildPortfolioWithPosition(Position... positions) {
        var portfolio = new Portfolio();
        Arrays.stream(positions).forEach(portfolio::add);
        return portfolio;
    }
}
