package com.kouasseu.portfolio.firstattempt;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 04:19<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class Portfolio {

    List<Stock> stocks = new ArrayList<>();
    public void printPortfolio() {
        stocks.forEach(System.out::println);
    }

    public double totalValue() {
        if(stocks == null) {
            return 0;
        }
        return stocks.stream()
                .mapToDouble(Stock::totalValue)
                .sum();
    }

    public void add(Stock symbol) {
        Optional<Stock> mathStock = stocks.stream().filter(stock -> stock.symbol().equals(symbol.symbol()))
                .findFirst();

        mathStock.ifPresentOrElse(stock -> {
            int totalQty = stock.qty() + symbol.qty();
            var newStock = new Stock(stock.symbol(), totalQty, (stock.px() + symbol.px())/totalQty);

            this.stocks.removeIf(stock1 -> stock1.symbol().equals(symbol.symbol()));
            this.stocks.add(newStock);
        }, () -> this.stocks.add(symbol));
    }

}
