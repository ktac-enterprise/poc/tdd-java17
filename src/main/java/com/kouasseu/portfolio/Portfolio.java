package com.kouasseu.portfolio;

import java.util.*;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 05:37<br></br>
 * By : @author alexk<br></br>
 * Project : tdd-java17<br></br>
 * Package : com.kouasseu.portfolio<br></br>
 */
public class Portfolio {

    private final Map<String, Position> positions;

    public Portfolio() {
        this.positions = new HashMap<>();
    }

    public Map<String, Position> getAllPositions() {
        return positions;
    }

    public void add(Position position) {
        if(isPositionValueAreValid(position)) {
            throw new IllegalArgumentException(String.format("You have provide a position with invalid value -->> %s", position));
        }
        String symbol = position.getStock().symbol();
        if(positions.containsKey(symbol)) {
            Position existingPosition = positions.get(symbol);
            int newQuantity = existingPosition.getQty() + position.getQty();
            double averagePrice = (existingPosition.getPx() * existingPosition.getQty() + position.getPx() * position.getQty()) / newQuantity;
            existingPosition.setQuantity(newQuantity);
            existingPosition.setPrice(averagePrice);
        } else {
            positions.put(symbol, position);
        }
    }

    private static boolean isPositionValueAreValid(Position position) {
        return position == null || position.getStock() == null || position.getStock().symbol() == null || position.getQty() == 0 || position.getPx() == 0;
    }

    public Position getPosition(String symbol) {
        return positions.get(symbol);
    }

    public double getTotalValue() {
        return positions.values().stream()
                .mapToDouble(Position::getValue)
                .sum();
    }


    public void print() {
        positions.values().forEach(System.out::println);

        System.out.println("=======================================");
        System.out.println("Total Value: " + getTotalValue());
    }

    public int getPositionsSize() {
        return positions.size();
    }

    public void remove(String stock, int qty, int px) {
        if(stock == null || stock.isEmpty() || stock.isBlank()) {
            throw new IllegalArgumentException(String.format("You have provided an invalid stock information -->> %s", stock));
        }
        if (positions.containsKey(stock)) {
            Position position = positions.get(stock);
            if (qty == 0 || px == 0 || qty > position.getQty()) {
                throw new IllegalArgumentException(String.format("You have provided an invalid information -->> Qty: %s | Px: %S, actual position: %s", qty, px, position));
            }
            int newQuantity = position.getQty() - qty;
            if (newQuantity == 0) {
                positions.remove(stock);
                return;
            }
            double averagePrice = Math.abs(position.getPx() * position.getQty() - px * qty) / newQuantity;
            position.setQuantity(newQuantity);
            position.setPrice(averagePrice);
        } else {
            throw new IllegalArgumentException(String.format("The stock you have provided is not found -->> %s", stock));
        }

    }
}
