package com.kouasseu;

/**
 * Copyright (c) 2023, Alex K., All Right Reserved.<br></br>
 * <a href="https://www.linkedin.com/in/alex-kouasseu/">My LinkedIn Account</a><br></br>
 * -----------------------------------<br></br>
 * When :  02/09/2023 -- 04:10<br></br>
 * By : @author alexk<br></br>
 * Project : Default (Template) Project<br></br>
 * Package : org.example<br></br>
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}